library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity circuito_exp5 is
    port (
        clock : in std_logic;
        reset : in std_logic;
        iniciar : in std_logic;
        chaves : in std_logic_vector (3 downto 0);
        acertou : out std_logic;
        errou : out std_logic;
        pronto : out std_logic;
        db_igual : out std_logic;
        db_contagem : out std_logic_vector (6 downto 0);
        db_memoria : out std_logic_vector (6 downto 0);
        db_estado : out std_logic_vector (6 downto 0);
        db_jogada : out std_logic_vector (6 downto 0);
        db_tem_jogada : out std_logic;
        db_clock : out std_logic
    );
end entity;

architecture arch of circuito_exp5 is

    component hexa7seg
        port (
            hexa : in  std_logic_vector(3 downto 0);
            sseg : out std_logic_vector(6 downto 0)
        );
    end component;

    component fluxo_dados
        port (
            clock		    : in  std_logic;
            zeraC		    : in  std_logic;
            contaC		  : in  std_logic;
            zeraR		    : in  std_logic;
            enableR		  : in  std_logic;
            escreveM    : in  std_logic;
            chaves		  : in  std_logic_vector (3 downto 0);
            igual		    : out std_logic;
            fimC        : out std_logic;
            jogada_feita    : out std_logic;
            db_tem_jogada   : out std_logic;
            db_contagem : out std_logic_vector (3 downto 0);
            db_memoria  : out std_logic_vector (3 downto 0);
            db_jogada : out std_logic_vector (3 downto 0)
        );
    end component;

    component unidade_controle
        port ( 
            clock:     in  std_logic; 
            reset:     in  std_logic; 
            iniciar:   in  std_logic;
            fim:       in  std_logic;
            igual:     in  std_logic;
            jogada:    in  std_logic;
            pronto:    out std_logic;
            zera:      out std_logic;
            registra:  out std_logic;
            conta:     out std_logic;
            acertou:   out std_logic;
            errou:     out std_logic;
            db_estado: out std_logic_vector(3 downto 0)
        );
    end component;

    signal ck, db_fim, db_zera, db_conta, db_i, db_registra, db_jogada_feita: std_logic;
    signal db_mem4, db_conta4, db_estado4, db_jogada4: std_logic_vector (3 downto 0);
begin

    HEX0: hexa7seg port map(
        hexa => db_jogada4,
        sseg => db_jogada
    );

    HEX1: hexa7seg port map(
        hexa => db_mem4,
        sseg => db_memoria
    );

    HEX2: hexa7seg port map(
        hexa => db_conta4,
        sseg => db_contagem
    );

    HEX3: hexa7seg port map(
        hexa => db_estado4,
        sseg => db_estado
    );

    UC: unidade_controle port map (
        jogada => db_jogada_feita,
        registra => db_registra,
        clock => ck,
        reset => reset,
        iniciar => iniciar,
        igual => db_i,
        fim => db_fim,
        zera => db_zera,
        conta => db_conta,
        acertou => acertou,
        errou => errou,
        db_estado => db_estado4,
		pronto => pronto
    );

    FD: fluxo_dados port map(
        clock => ck,
        zeraC => db_zera,
        contaC => db_conta,
        zeraR => db_zera,
        enableR => db_registra,
        escreveM => '1',
        chaves => chaves,
        igual => db_i,
        fimC => db_fim,
        jogada_feita => db_jogada_feita,
        db_tem_jogada => db_tem_jogada,
        db_contagem => db_conta4,
        db_memoria => db_mem4,
        db_jogada => db_jogada4
    );

    db_clock <= ck;
    db_igual <= db_i;
    ck <= clock;
end arch;