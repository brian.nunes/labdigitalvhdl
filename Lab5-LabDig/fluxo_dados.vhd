library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity fluxo_dados is
  port (
        clock		    : in  std_logic;
        zeraC		    : in  std_logic;
        contaC		  : in  std_logic;
        zeraR		    : in  std_logic;
        enableR		  : in  std_logic;
        escreveM    : in  std_logic;
        chaves		  : in  std_logic_vector (3 downto 0);
        igual		    : out std_logic;
        fimC        : out std_logic;
        jogada_feita    : out std_logic;
        db_tem_jogada   : out std_logic;
        db_contagem : out std_logic_vector (3 downto 0);
        db_memoria  : out std_logic_vector (3 downto 0);
        db_jogada  : out std_logic_vector (3 downto 0)
);
end fluxo_dados;

architecture structural of fluxo_dados is

  component registrador_4bits
    port (       
      clock          : in  std_logic;
      D     : in  std_logic_vector(3 downto 0);
      clear          : in  std_logic;
      enable           : in  std_logic;
      Q   : out std_logic_vector(3 downto 0)
    );
  end component;

  component edge_detector
    port (
        clock  : in  std_logic;
        reset  : in  std_logic;
        sinal  : in  std_logic;
        pulso  : out std_logic
      );
  end component;

  component ram_16x4
    port(
      clk       : in  std_logic;
      endereco     : in  std_logic_vector(3 downto 0);
      dado_entrada : in  std_logic_vector(3 downto 0);
      we           : in  std_logic;
      ce           : in  std_logic;
      dado_saida   : out std_logic_vector(3 downto 0)
    );
  end component;

  component comparador_85
    port(
	    i_A3   : in  std_logic;
      i_B3   : in  std_logic;
      i_A2   : in  std_logic;
      i_B2   : in  std_logic;
      i_A1   : in  std_logic;
      i_B1   : in  std_logic;
      i_A0   : in  std_logic;
      i_B0   : in  std_logic;
      i_AGTB : in  std_logic;
      i_ALTB : in  std_logic;
      i_AEQB : in  std_logic;
      o_AGTB : out std_logic;
      o_ALTB : out std_logic;
      o_AEQB : out std_logic
  	);
  end component;
  
  component contador_163
    port(
	    clock : in  std_logic;
      clr   : in  std_logic;
      ld    : in  std_logic;
      ent   : in  std_logic;
      enp   : in  std_logic;
      D     : in  std_logic_vector (3 downto 0);
      Q     : out std_logic_vector (3 downto 0);
      rco   : out std_logic 
	  );
  end component;

  signal sinal_chave, chaves_of, contagem, db_mem: std_logic_vector (3 downto 0);
  signal not_reset, ck, or_chaves, resetOF: std_logic;
begin
  chaves_of <= chaves;

  REG: registrador_4bits port map(
    clock => ck,
    D => chaves_of,
    clear => zeraR,
    enable => enableR,
    Q => sinal_chave
  );

  EDGE: edge_detector port map(
    clock  => ck,
    reset  => resetOF,
    sinal  => or_chaves,
    pulso  => jogada_feita
  );

  RAM: entity work.ram_16x4(ram_modelsim) port map (
      clk       => ck,
      endereco     => contagem,
      dado_entrada => chaves_of,
      we           => escreveM,
      ce           => '0',
      dado_saida   => db_mem
  );

  CMP: comparador_85 port map (
    i_A3 => db_mem(3),
	  i_B3 => sinal_chave(3),
    i_A2 => db_mem(2),
	  i_B2 => sinal_chave(2),
    i_A1 => db_mem(1),
	  i_B1 => sinal_chave(1),
    i_A0 => db_mem(0),
	  i_B0 => sinal_chave(0),
	  i_AGTB => '0',
	  i_ALTB => '0',
	  i_AEQB => '1',
	  o_AGTB => open,
	  o_ALTB => open,
	  o_AEQB => igual
  );
  
  COUNT: contador_163 port map (
	  clock => ck,
    clr   => not_reset,
	  ld    => '1',
	  ent   => '1',
	  enp   => contaC,
	  D     => "0000",
    Q     => contagem,
	  rco   => fimC
  );

  or_chaves <= chaves_of(0) or chaves_of(1) or chaves_of(2) or chaves_of(3);

  db_tem_jogada <= or_chaves;
  ck <= clock;
  db_memoria <= db_mem;
  db_contagem <= contagem;
  resetOF <= zeraC;
  not_reset <= not(resetOF);
  db_jogada <= sinal_chave;
end architecture;