------------------------------------------------------------------
-- Arquivo   : unidade_controle.vhd
-- Projeto   : Experiencia 04 - Projeto de uma Unidade de Controle
------------------------------------------------------------------
-- Descricao : codificação da máquina de estados em VHDL 
--             > para mais informação, consulte o livro
--               VHDL Descricao e Sintese de Circuitos Digitais
--               Roberto D'Amore, LTC Editora.
--               ou acesse a postagem do prof, Bruno Albertini em
--               https://balbertini.github.io/vhdl_fsm-pt_BR.html
------------------------------------------------------------------
-- Revisoes  :
--     Data        Versao  Autor             Descricao
--     21/01/2020  1.0     Edson Midorikawa  criacao
------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity unidade_controle is 
  port ( 
    clock:     in  std_logic; 
    reset:     in  std_logic; 
    iniciar:   in  std_logic;
    fim:       in  std_logic;
    igual:     in  std_logic;
    jogada:    in  std_logic;
    timeout:   in  std_logic;       --flag de timeout
    conta_timer:   out  std_logic;   --flag de controle de contagem
    reset_timer:    out std_logic;  --sinal de reset do timer
    pronto:    out std_logic;
    zera:      out std_logic;
    registra:  out std_logic;
    conta:     out std_logic;
    acertou:   out std_logic;
    errou:     out std_logic;
    db_estado: out std_logic_vector(3 downto 0)
  );
end entity;

architecture fsm of unidade_controle is
  type t_estado is (A, B, C, D, E, F, G, H);
  signal Eatual, Eprox: t_estado;
begin
  -- memoria de estado
  process (clock,reset)
  begin
    if reset='1' then
      Eatual <= A;
    elsif clock'event and clock = '1' then
      Eatual <= Eprox; 
    end if;
  end process;

  -- logica de proximo estado
  Eprox <=
      A when  Eatual=A and iniciar='0' else
      B when  Eatual=A and iniciar='1' else
      C when  Eatual=B else
      C when  Eatual=C and jogada='0' and timeout = '0' else --estado C considera o timeout agora
      H when  Eatual=C and jogada='0' and timeout = '1' else --perde quando timeout é acionado
      D when  Eatual=C and jogada='1'else
      E when  Eatual=D else
      F when  Eatual=E and fim='0' and igual='1' else
      G when  Eatual=E and fim='1' and igual='1' else
      H when  Eatual=E and fim='0' and igual='0' else
      H when  Eatual=E and fim='1' and igual='0' else
      C when  Eatual=F else
      B when  Eatual=G and iniciar='1' else
      G when  Eatual=G and iniciar='0' else
      B when  Eatual=H and iniciar='1'else
      H when  Eatual=H and iniciar='0' else
      B;

  -- logica de saída (maquina de Moore)
  with Eatual select --controle de reset = ocorre sempre q uma jogada é feita ou que o jogo é zerado
    reset_timer <=   '0' when A | C | E | F | G | H,
                     '1' when D | B,
                     '0' when others;

  with Eatual select --controla o contador do timeout, que funciona apenas no estado C
    conta_timer <=   '0' when A | B | E | F | D | G | H,
                  '1' when C,
                  '0' when others;

  with Eatual select
    registra <=   '0' when A | B | C | E | F | G | H,
                  '1' when D,
                  '0' when others;

  with Eatual select
    zera <=   '0' when A | C | D | E | F | G | H,
              '1' when B,
              '0' when others;

  with Eatual select
    conta <=  '0' when A | B | C | D | E | G | H,
              '1' when F,
              '0' when others;

  with Eatual select
    acertou <= '0' when A | B | C | D | E | F | H,
              '1' when G,
              '0' when others;

  with Eatual select
    errou <= '0' when A | B | C | D | E | F | G,
              '1' when H,
              '0' when others;
  
  with Eatual select
    pronto <= '0' when A | B | C | D | E | F,
              '1' when G | H,
              '0' when others;

  -- saida de depuracao (db_estado)
  with Eatual select
    db_estado <= "0000" when A,      -- 0
                 "1011" when B,      -- B
                 "1100" when C,      -- C
                 "1101" when D,      -- D
                 "1110" when E,      -- E
                 "1111" when F,      -- F
                 "0001" when G,      -- 1
                 "0010" when H,      -- 2
                 "0011" when others; -- 3
end fsm;    