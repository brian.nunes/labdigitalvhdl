library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity contador_163 is
   port (
        clock : in  std_logic;
        clr   : in  std_logic;
        ld    : in  std_logic;
        ent   : in  std_logic;
        enp   : in  std_logic;
        D     : in  std_logic_vector (4 downto 0);
        Q     : out std_logic_vector (4 downto 0);
        rco   : out std_logic 
   );
end contador_163;

architecture comportamental of contador_163 is
  signal IQ: integer range 0 to 31;
begin
  
  process (clock,IQ)
  begin

    if clock'event and clock='1' then
      if clr='0' then   IQ <= 0; 
      elsif ld='0' then IQ <= to_integer(unsigned(D));
      elsif ent='1' and enp='1' then
        if IQ=31 then   IQ <= 0; 
        else            IQ <= IQ + 1; 
        end if;
      else              IQ <= IQ;
      end if;
    end if;
    
    if IQ=31 and ent='1' then rco <= '1'; 
    else                      rco <= '0'; 
    end if;

    Q <= std_logic_vector(to_unsigned(IQ, Q'length));

  end process;
end comportamental;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity comparador_85 is
  port (
    i_A4   : in  std_logic;
    i_B4   : in  std_logic;
    i_A3   : in  std_logic;
    i_B3   : in  std_logic;
    i_A2   : in  std_logic;
    i_B2   : in  std_logic;
    i_A1   : in  std_logic;
    i_B1   : in  std_logic;
    i_A0   : in  std_logic;
    i_B0   : in  std_logic;
    i_AGTB : in  std_logic;
    i_ALTB : in  std_logic;
    i_AEQB : in  std_logic;
    o_AGTB : out std_logic;
    o_ALTB : out std_logic;
    o_AEQB : out std_logic
  );
end entity comparador_85;

architecture dataflow of comparador_85 is
  signal agtb : std_logic;
  signal aeqb : std_logic;
  signal altb : std_logic;
begin
  -- equacoes dos sinais: pagina 462, capitulo 6 do livro-texto
  -- Wakerly, J.F. Digital Design - Principles and Practice, 4th Edition
  -- veja tambem datasheet do CI SN7485 (Function Table) 
  agtb <= (i_A4 and not(i_B4)) or
          (not(i_A4 xor i_B4) and i_A3 and not(i_B3)) or
          (not(i_A4 xor i_B4) and not(i_A3 xor i_B3) and i_A2 and not(i_B2)) or
          (not(i_A4 xor i_B4) and not(i_A3 xor i_B3) and not(i_A2 xor i_B2) and not(i_A1 xor i_B1)) or
          (not(i_A4 xor i_B4) and not(i_A3 xor i_B3) and not(i_A2 xor i_B2) and not(i_A1 xor i_B1) and i_A0 and not(i_B0));
  aeqb <= not((i_A4 xor i_B4) or (i_A3 xor i_B3) or (i_A2 xor i_B2) or (i_A1 xor i_B1) or (i_A0 xor i_B0));
  altb <= not(agtb or aeqb);
  -- saidas
  o_AGTB <= agtb or (aeqb and (not(i_AEQB) and not(i_ALTB)));
  o_ALTB <= altb or (aeqb and (not(i_AEQB) and not(i_AGTB)));
  o_AEQB <= aeqb and i_AEQB;
  
end architecture dataflow;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity circuito_exp2 is
  port (
    clock		: in  std_logic;
	  reset		: in  std_logic;
	  enable		: in  std_logic;
	  chaves		: in  std_logic_vector (4 downto 0);
	  igual		: out std_logic;
	  db_contagem : out std_logic_vector (4 downto 0)
);
end circuito_exp2;

architecture structural of circuito_exp2 is

  component comparador_85
    port(
      i_A4   : in  std_logic;
      i_B4   : in  std_logic;
	    i_A3   : in  std_logic;
      i_B3   : in  std_logic;
      i_A2   : in  std_logic;
      i_B2   : in  std_logic;
      i_A1   : in  std_logic;
      i_B1   : in  std_logic;
      i_A0   : in  std_logic;
      i_B0   : in  std_logic;
      i_AGTB : in  std_logic;
      i_ALTB : in  std_logic;
      i_AEQB : in  std_logic;
      o_AGTB : out std_logic;
      o_ALTB : out std_logic;
      o_AEQB : out std_logic
	);
  end component;
  
  component contador_163
    port(
	    clock : in  std_logic;
      clr   : in  std_logic;
      ld    : in  std_logic;
      ent   : in  std_logic;
      enp   : in  std_logic;
      D     : in  std_logic_vector (4 downto 0);
      Q     : out std_logic_vector (4 downto 0);
      rco   : out std_logic 
	);
  end component;

  signal contagem: std_logic_vector (4 downto 0);
  signal saida, gt, eq: std_logic;
begin

  CMP: comparador_85 port map (
    i_A4 => contagem(4),
	  i_B4 => chaves(4),
    i_A3 => contagem(3),
	  i_B3 => chaves(3),
    i_A2 => contagem(2),
	  i_B2 => chaves(2),
    i_A1 => contagem(1),
	  i_B1 => chaves(1),
    i_A0 => contagem(0),
	  i_B0 => chaves(0),
	  i_AGTB => '0',
	  i_ALTB => '0',
	  i_AEQB => '1',
	  o_AGTB => gt,
	  o_ALTB => open,
	  o_AEQB => eq
  );
  
  COUNT: contador_163 port map (
	  clock => clock,
    clr   => reset,
	  ld    => '1',
	  ent   => '0',
	  enp   => enable,
	  D     => "00000",
    Q     => contagem,
	  rco   => open
  );

  db_contagem <= contagem;
  igual <= gt or eq;

end architecture;