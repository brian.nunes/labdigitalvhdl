library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity fluxo_dados is
  port (
        clock		    : in  std_logic;
        zeraL 	    : in  std_logic;
        zeraTimer 	    : in  std_logic;
        contaTimer 	    : in  std_logic;
        fimTimer    : out std_logic;
        contaL		  : in  std_logic;
        resetGeral  : in  std_logic;
        zeraE 	    : in  std_logic;
        contaE		  : in  std_logic;
        escreve     : in  std_logic;
        botoes		  : in  std_logic_vector (3 downto 0);
        registraR	  : in  std_logic;
        zeraR		    : in  std_logic;
        fimL        : out std_logic;
        fimE        : out std_logic;
        jogada_feita  : out std_logic;
        db_limite  : out std_logic_vector (3 downto 0);
        db_jogada  : out std_logic_vector (3 downto 0);
        db_memoria  : out std_logic_vector (3 downto 0);
        db_contagem : out std_logic_vector (3 downto 0);
        chavesIgualMemoria		    : out std_logic;  
        enderecoIgualOuMenorLimite		    : out std_logic;
        enderecoIgualLimite		    : out std_logic    
);
end fluxo_dados;

architecture structural of fluxo_dados is

  component contador_5seg
    port (
       clock : in  std_logic;
       clr   : in  std_logic;
       ld    : in  std_logic;
       ent   : in  std_logic;
       enp   : in  std_logic;
       D     : in  std_logic_vector (12 downto 0);
       Q     : out std_logic_vector (12 downto 0);
       rco   : out std_logic 
    );
  end component;
  
  component registrador_4bits
    port (       
      clock          : in  std_logic;
      D     : in  std_logic_vector(3 downto 0);
      clear          : in  std_logic;
      enable           : in  std_logic;
      Q   : out std_logic_vector(3 downto 0)
    );
  end component;

  component edge_detector
    port (
        clock  : in  std_logic;
        reset  : in  std_logic;
        sinal  : in  std_logic;
        pulso  : out std_logic
      );
  end component;

  component ram_16x4
    port(
      clk       : in  std_logic;
      endereco     : in  std_logic_vector(3 downto 0);
      dado_entrada : in  std_logic_vector(3 downto 0);
      we           : in  std_logic;
      ce           : in  std_logic;
      dado_saida   : out std_logic_vector(3 downto 0)
    );
  end component;

  component comparador_85
    port(
	    i_A3   : in  std_logic;
      i_B3   : in  std_logic;
      i_A2   : in  std_logic;
      i_B2   : in  std_logic;
      i_A1   : in  std_logic;
      i_B1   : in  std_logic;
      i_A0   : in  std_logic;
      i_B0   : in  std_logic;
      i_AGTB : in  std_logic;
      i_ALTB : in  std_logic;
      i_AEQB : in  std_logic;
      o_AGTB : out std_logic;
      o_ALTB : out std_logic;
      o_AEQB : out std_logic
  	);
  end component;
  
  component contador_163
    port(
	    clock : in  std_logic;
      clr   : in  std_logic;
      ld    : in  std_logic;
      ent   : in  std_logic;
      enp   : in  std_logic;
      D     : in  std_logic_vector (3 downto 0);
      Q     : out std_logic_vector (3 downto 0);
      rco   : out std_logic 
	  );
  end component;

  signal db_contagemtimer: std_logic_vector (12 downto 0);
  signal s_jogada, chaves_of, s_endereco, s_dado, s_limite: std_logic_vector (3 downto 0);
  signal ck, or_chaves, or_chaves_f, resetOF, not_zeraE, not_zeraL, s_zeraE: std_logic;
begin

  TIMER: contador_5seg port map( --adicionado componente
    clock => ck,
    clr   => zeraTimer,     --recebe o reset da UC
    ld    => '1',
    ent   => '1',
    enp   => contaTimer,             --fica em alto sempre que esta esperando uma jogada da memoria
    D     => "0000000000000",
    Q     => db_contagemtimer,
    rco   => fimTimer      --retorna o fim para o sinal que vai para a UC
  );

  RegBotoes: registrador_4bits port map(
    clock => ck,
    D => chaves_of,
    clear => zeraR,
    enable => registraR,
    Q => s_jogada
  );

  EDGE: edge_detector port map(
    clock  => ck,
    reset  => resetGeral,
    sinal  => or_chaves_f,
    pulso  => jogada_feita
  );

  MemJogada: entity work.ram_16x4(ram_modelsim) port map (
      clk       => ck,
      endereco     => s_endereco,
      dado_entrada => s_jogada,
      we           => escreve,
      ce           => '0',
      dado_saida   => s_dado
  );

  CompJog: comparador_85 port map (
    i_A3 => s_dado(3),
	  i_B3 => s_jogada(3),
    i_A2 => s_dado(2),
	  i_B2 => s_jogada(2),
    i_A1 => s_dado(1),
	  i_B1 => s_jogada(1),
    i_A0 => s_dado(0),
	  i_B0 => s_jogada(0),
	  i_AGTB => '0',
	  i_ALTB => '0',
	  i_AEQB => '1',
	  o_AGTB => open,
	  o_ALTB => open,
	  o_AEQB => chavesIgualMemoria
  );

  CompLmt: comparador_85 port map (
    i_A3 => s_limite(3),
	  i_B3 => s_endereco(3),
    i_A2 => s_limite(2),
	  i_B2 => s_endereco(2),
    i_A1 => s_limite(1),
	  i_B1 => s_endereco(1),
    i_A0 => s_limite(0),
	  i_B0 => s_endereco(0),
	  i_AGTB => '1',
	  i_ALTB => '0',
	  i_AEQB => '1',
	  o_AGTB => open,
	  o_ALTB => enderecoIgualOuMenorLimite,
	  o_AEQB => enderecoIgualLimite
  );
  
  CountEnd: contador_163 port map (
	  clock => ck,
    clr   => not_zeraE,
	  ld    => '1',
	  ent   => '1',
	  enp   => contaE,
	  D     => "0000",
    Q     => s_endereco,
	  rco   => fimE
  );

  CountLmt: contador_163 port map (
	  clock => ck,
    clr   => not_zeraL,
	  ld    => '1',
	  ent   => '1',
	  enp   => contaL,
	  D     => "0000",
    Q     => s_limite,
	  rco   => fimL
  );

  with or_chaves select
    or_chaves_f <= '0' when 'U',
                   '0' when '0',
						 '1' when '1',
						 '1' when others;

  or_chaves <= chaves_of(0) or chaves_of(1) or chaves_of(2) or chaves_of(3);
  not_zeraL <= not(zeraL);
  s_zeraE <= zeraE;
  not_zeraE <= not(zeraE);
  chaves_of <= botoes;
  ck <= clock;
  db_limite <= s_limite;
  db_memoria <= s_dado;
  db_contagem <= s_endereco;
  db_jogada <= s_jogada;
end architecture;