library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity circuito_exp7 is
    port (
        clock : in std_logic;
        reset : in std_logic;
        jogar : in std_logic;
        repete : in std_logic;
        botoes : in std_logic_vector (3 downto 0);
        leds : out std_logic_vector (3 downto 0);
        db_memoria : out std_logic_vector (3 downto 0);
        db_contagem : out std_logic_vector (3 downto 0);
        db_jogada : out std_logic_vector (3 downto 0);
        espera : out std_logic;
        pronto : out std_logic;
        ganhou : out std_logic;
        perdeu : out std_logic
    );
end entity;

architecture arch of circuito_exp7 is

    component hexa7seg
        port (
            hexa : in  std_logic_vector(3 downto 0);
            sseg : out std_logic_vector(6 downto 0)
        );
    end component;

    component fluxo_dados
        port (
            clock		    : in  std_logic;
            zeraL 	    : in  std_logic;
            contaL		  : in  std_logic;
            zeraTimer 	    : in  std_logic;
            contaTimer 	    : in  std_logic;
            fimTimer    : out std_logic;
            zeraE 	    : in  std_logic;
            resetGeral  : in  std_logic;
            contaE		  : in  std_logic;
            zeraR		    : in  std_logic;
            escreve     : in  std_logic;
            botoes		  : in  std_logic_vector (3 downto 0);
            registraR	  : in  std_logic;
            fimL        : out std_logic;
            fimE        : out std_logic;
            jogada_feita  : out std_logic;
            db_limite  : out std_logic_vector (3 downto 0);
            db_jogada  : out std_logic_vector (3 downto 0);
            db_memoria  : out std_logic_vector (3 downto 0);
            db_contagem : out std_logic_vector (3 downto 0);
            chavesIgualMemoria		    : out std_logic;  
            enderecoIgualOuMenorLimite		    : out std_logic;
            enderecoIgualLimite		    : out std_logic 
        );
    end component;

    component unidade_controle
        port ( 
            clock:     in  std_logic; 
            reset:     in  std_logic; 
            jogar:   in  std_logic;
            igualContELmt:       in  std_logic;
            repete:       in  std_logic;
            zeraTimer 	    : out  std_logic;
            contaTimer 	    : out  std_logic;
            fimTimer    : in std_logic;
            fimL:       in  std_logic;
            igual:     in  std_logic;
            uc_escreve:    out std_logic;
            jogada:    in  std_logic;
            pronto:    out std_logic;
            espera:    out std_logic;
            zeraL:      out std_logic;
            zeraE:      out std_logic;
            zeraR:      out std_logic;
            registra:  out std_logic;
            contaE:     out std_logic;
            contaL:     out std_logic;
            acertou:   out std_logic;
            errou:     out std_logic;
            db_estado: out std_logic_vector(3 downto 0)
        );
    end component;

    signal s_zeraTimer, s_contaTimer, s_fimTimer, ck, db_fimL, db_fimE, db_zeraR, db_zeraE, db_zeraL, db_contaL, s_uc_escreve, db_contaE, db_i, db_registra, db_jogada_feita, db_endI, db_endMouI, s_reset: std_logic;
    signal db_mem4, db_conta4, db_estado4, db_chaves_or, db_or_geral, db_jogada4, db_lmt, db_botoes, saida_leds, valor_memoria: std_logic_vector (3 downto 0);
    signal db_mem6, db_conta6, db_jogada6: std_logic_vector(6 downto 0);
begin
    db_memoria <= db_mem6;
    db_jogada <= db_jogada6;
    db_contagem <= db_conta6;

    HEX1: hexa7seg port map(
        hexa => db_mem4,
        sseg => db_mem6
    );

    HEX2: hexa7seg port map(
        hexa => db_conta4,
        sseg => db_conta6
    );

    HEX2: hexa7seg port map(
        hexa => db_jogada4,
        sseg => db_jogada6
    );

    UC: unidade_controle port map (    
        jogada => db_jogada_feita,
        registra => db_registra,
        repete => repete,
        clock => ck,
        reset => s_reset,
        jogar => jogar,
        igual => db_i,
        zeraTimer => s_zeraTimer,
        contaTimer => s_contaTimer,
        fimTimer => s_fimTimer,
        espera => espera,
        igualContELmt => db_endI,
        uc_escreve => s_uc_escreve,
        fimL => db_fimL,
        zeraL => db_zeraL,
        zeraE => db_zeraE,
        zeraR => db_zeraR,
        contaE => db_contaE,
        contaL => db_contaL,
        acertou => ganhou,
        errou => perdeu,
        db_estado => db_estado4,
		pronto => pronto
    );

    FD: fluxo_dados port map(
        clock => ck,
        zeraL => db_zeraL,
        zeraTimer => s_zeraTimer,
        contaTimer => s_contaTimer,
        fimTimer => s_fimTimer,
        contaL => db_contaL,
        fimL => db_fimL,
        zeraE => db_zeraE,
        contaE => db_contaE,
        resetGeral => s_reset,
        fimE => db_fimE,
        zeraR => db_zeraR,
        registraR => db_registra,
        escreve => s_uc_escreve,
        botoes => db_botoes,
        chavesIgualMemoria => db_i,
        jogada_feita => db_jogada_feita,
        db_limite => db_lmt,
        db_contagem => db_conta4,
        db_memoria => db_mem4,
        db_jogada => db_jogada4,
        enderecoIgualOuMenorLimite => db_endMouI,
        enderecoIgualLimite	=> db_endI
    );

    with db_estado4 select
        saida_leds <=   valor_memoria when "1111",
                        db_botoes when others;
    
    valor_memoria<=db_mem4;
    s_reset <= reset;
    db_botoes <= botoes;
    leds <= saida_leds;
    ck <= clock;
end arch;