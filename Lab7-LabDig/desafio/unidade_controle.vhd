------------------------------------------------------------------
-- Arquivo   : unidade_controle.vhd
-- Projeto   : Experiencia 04 - Projeto de uma Unidade de Controle
------------------------------------------------------------------
-- Descricao : codificação da máquina de estados em VHDL 
--             > para mais informação, consulte o livro
--               VHDL Descricao e Sintese de Circuitos Digitais
--               Roberto D'Amore, LTC Editora.
--               ou acesse a postagem do prof, Bruno Albertini em
--               https://balbertini.github.io/vhdl_fsm-pt_BR.html
------------------------------------------------------------------
-- Revisoes  :
--     Data        Versao  Autor             Descricao
--     21/01/2020  1.0     Edson Midorikawa  criacao
------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity unidade_controle is 
  port ( 
    clock:     in  std_logic; 
    reset:     in  std_logic; 
    jogar:   in  std_logic;
    repete:   in  std_logic;
    zeraTimer 	    : out  std_logic;
    contaTimer 	    : out  std_logic;
    fimTimer    : in std_logic;
    igualContELmt:       in  std_logic;
    fimL:       in  std_logic;
    igual:     in  std_logic;
    jogada:    in  std_logic;
    pronto:    out std_logic;
    espera:    out std_logic;
    uc_escreve:    out std_logic;
    zeraL:      out std_logic;
    zeraE:      out std_logic;
    zeraR:      out std_logic;
    registra:  out std_logic;
    contaE:     out std_logic;
    contaL:     out std_logic;
    acertou:   out std_logic;
    errou:     out std_logic;
    db_estado: out std_logic_vector(3 downto 0)
  );
end entity;

architecture fsm of unidade_controle is
  component contador_1seg
    port (
         clock : in  std_logic;
         clr   : in  std_logic;
         ld    : in  std_logic;
         ent   : in  std_logic;
         enp   : in  std_logic;
         D     : in  std_logic_vector (12 downto 0);
         Q     : out std_logic_vector (12 downto 0);
         rco   : out std_logic 
    );
 end component;

  type t_estado is (A, B, C, D, E, F, G, H, I, J, K, L, M, N, S, Q, W, X, Z);
  signal Eatual, Eprox: t_estado;
  signal ck, zeraMostrador, contaMostrador, fimMostrador: std_logic;
  signal db_contagemMostrador: std_logic_vector(12 downto 0);
begin
  ck <= clock;

  TIMER: contador_1seg port map( --adicionado componente
    clock => ck,
    clr   => zeraMostrador,     --recebe o reset da UC
    ld    => '1',
    ent   => '1',
    enp   => contaMostrador,             --fica em alto sempre que esta esperando uma jogada da memoria
    D     => "0000000000000",
    Q     => db_contagemMostrador,
    rco   => fimMostrador     --retorna o fim para o sinal que vai para a UC
  );

  -- memoria de estado
  process (clock,reset)
  begin
    if reset='1' then
      Eatual <= A;
    elsif clock'event and clock = '1' then
      Eatual <= Eprox; 
    end if;
  end process;

  -- logica de proximo estado
  Eprox <=
      A when  Eatual=A and jogar='0' else
      B when  Eatual=A and jogar='1' else
      S when  Eatual=B else
      S when  Eatual=S and jogada='0' and fimTimer='0' else
      S when  Eatual=S and jogada='U' and fimTimer='0' else
      H when  Eatual=S and jogada='0' and fimTimer='1' else
      H when  Eatual=S and jogada = 'U' and fimTimer='1' else
      Q when  Eatual=S and jogada='1' and fimTimer='0' else
      C when  Eatual=Q else
      D when  Eatual=C else
      D when  Eatual=D and jogada='0' and fimTimer='0' else
      D when  Eatual=D and jogada = 'U' and fimTimer='0' else
      H when  Eatual=D and jogada='0' and fimTimer='1' else
      H when  Eatual=D and jogada = 'U' and fimTimer='1' else
      E when  Eatual=D and jogada='1' else
      F when  Eatual=E else
      G when  Eatual=F and igualContELmt='0' and igual='1' else
      I when  Eatual=F and igualContELmt='1' and igual='1' else
      H when  Eatual=F and igualContELmt='0' and igual='0' else
      H when  Eatual=F and igualContELmt='1' and igual='0' else
      D when  Eatual=G else
      B when  Eatual=H and jogar='1' and repete='0' else
      H when  Eatual=H and jogar='0' and repete='0'else
      B when  Eatual=H and jogar='1' and repete='1' else
      W when  Eatual=H and jogar='0' and repete='1'else
      X when  Eatual=W else
      X when  Eatual=X and fimMostrador='0' else
      Z when  Eatual=X and fimMostrador='1' else
      X when  Eatual=Z and igualContELmt='0' else
      H when  Eatual=Z and igualContELmt='1' else
      N when  Eatual=I and fimL='1'else
      J when  Eatual=I and fimL='0' else
      K when  Eatual=J else
      L when  Eatual=K and jogada='1' and fimTimer='0' else
      H when  Eatual=K and jogada='0' and fimTimer='1' else
      H when  Eatual=K and jogada = 'U' and fimTimer='1' else
      K when  Eatual=K and jogada='U' and fimTimer='0' else
      K when  Eatual=K and jogada='0' and fimTimer='0' else
      M when  Eatual=L else
      B when  Eatual=N and jogar='1' else
      N when  Eatual=N and jogar='0' else
      C when  Eatual=M else
      B;

  -- logica de saída (maquina de Moore)
  with Eatual select
    zeraMostrador <=   '1' when A | B | C | D | F | G | H | I | J,
                       '0' when Z,
                       '1' when others;

  with Eatual select
    contaMostrador <=   '0' when A | B | C | D | F | G | H | I | J,
                       '1' when X,
                       '0' when others;

  with Eatual select
    espera <=   '0' when A | B | C | D | F | G | H | I | J,
                  '1' when S | K,
                  '0' when others;

  with Eatual select
    registra <=   '0' when A | B | C | D | F | G | H | I | J | K,
                  '1' when E | L | Q | S,
                  '0' when others;

  with Eatual select
    zeraL <=   '0' when A | C | D | E | F | G | H | I | J | K,
              '1' when B,
              '0' when others;

  with Eatual select
    zeraE <=   '0' when A | D | E | F | G | H | I | J | K,
              '1' when C | B | W,
              '0' when others;
  
  with Eatual select
    zeraR <=   '0' when A | D | E | F | G | H | I | J | K,
              '1' when C | B,
              '0' when others;

  with Eatual select
    contaE <=  '0' when A | B | C | D | E | F | H | I | K,
              '1' when G | J | Z,
              '0' when others;

  with Eatual select
    contaL <=  '0' when A | B | C | D | E | F | G | H | I | J,
              '1' when M,
              '0' when others;

  with Eatual select
    acertou <= '0' when A | B | C | D | E | F | G | H | I | K,
              '1' when N,
              '0' when others;

  with Eatual select
    errou <= '0' when A | B | C | D | E | F | G | I | J | K,
              '1' when H,
              '0' when others;
  
  with Eatual select
    pronto <= '0' when A | B | C | D | E | F | G | I | K,
              '1' when N | H,
              '0' when others;

    with Eatual select
    uc_escreve <= '1' when A | B | C | D | E | F | G | I | J | K,
              '0' when M | Q,
              '1' when others;

    with Eatual select --controle de reset = ocorre sempre q uma jogada é feita ou que o jogo é zerado
    zeraTimer <=   '1' when A | C | D | F | G | H | I | J,
                     '0' when E | B | M | Q,
                     '1' when others;

    with Eatual select --controla o contador do timeout, que funciona apenas no estado C
    contaTimer <=   '0' when A | B | C | E | F | G | H | I | J,
                  '1' when D | S | K,
                  '0' when others;

  -- saida de depuracao (db_estado)
  with Eatual select
    db_estado <= "0001" when A,      -- 1
                 "0010" when B,      -- 2
                 "0011" when C,      -- 3
                 "0100" when D,      -- 4
                 "0101" when E,      -- 5
                 "0110" when F,      -- 6
                 "0111" when G,      -- 7
                 "1000" when H,      -- 8
                 "1001" when I,      -- 9
                 "1010" when J,      -- A
                 "1011" when K,      -- B
                 "1100" when L,      -- C
                 "1101" when M,      -- D
                 "1110" when N,      -- E
                 "1111" WHEN X,      -- F
                 "0000" when Q,      -- 0
                 "0000" when others;
end fsm;   