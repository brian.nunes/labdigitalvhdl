library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_bit.all;

entity circuito_exp7_tb is
end entity;

architecture tb of circuito_exp7_tb is
	component circuito_exp7
	port(
		  -- Sinais de entrada
		  clock 	: 	in 		std_logic;
		  reset 	: 	in 		std_logic;
		  jogar 	: 	in 		std_logic;
		  botoes 	: 	in 		std_logic_vector(3 downto 0);
		  -- Sinais de saida
		  leds 		: 	out 	std_logic_vector(3 downto 0);
		  pronto 	: 	out 	std_logic;
		  ganhou	: 	out 	std_logic;
		  perdeu 	: 	out 	std_logic;
		  espera 	: 	out 	std_logic
	);
	end component;
	type arranjo_memoria is array(0 to 15) of std_logic_vector(3 downto 0);
	signal memoria : arranjo_memoria := 
		(
			"0100",
			"0010",
			"0100",
			"1000",
			"0100",
			"0010",
			"1000",
			"0001",
			"0010",
			"0010",
			"0100",
			"0100",
			"1000",
			"1000",
			"0001",
			"0100" 
		);
  
	constant TbPeriod 			: 	time 		:= 	1000000 ns; --1000000ns para 1kHz de clock
	signal TbSimulation 		: 	std_logic 	:= 	'0';
	signal TbButtonOnWait		: 	integer 	:= 	5;
	signal TbButtonOffWait		: 	integer 	:= 	7;
	signal TbZero				: 	std_logic_vector(3 downto 0) := "0000";

	signal clock, reset, jogar, pronto, ganhou, perdeu, espera : std_logic;
	signal botoes, leds 	: std_logic_vector(3 downto 0);

begin
	DUT: circuito_exp7 port map 
	(
		Clock		=>		clock,
		reset		=>		reset,
		jogar		=>		jogar,
		botoes		=>		botoes,
		leds		=>		leds,
		pronto		=>		pronto,
		ganhou		=>		ganhou,
		perdeu		=>		perdeu,
		espera		=>		espera
	);
	
	clock <= not clock after TbPeriod/2 when TbSimulation = '1' else '0';
	stimuli: process
	begin
		TbSimulation <= '1';
			
			-- Condicoes iniciais
			reset <= '1';
			wait for 2*TbPeriod;
			reset <= '0';
			wait for 2*TbPeriod;
			
			--Inicio do jogo
			jogar <= '1';
			wait for TbButtonOnWait*TbPeriod;
			jogar <= '0';
			wait for TbButtonOnWait*TbPeriod;
			
			--Teste ate o 3 e errar
			for i in 0 to 3 loop
				for j in 0 to i loop
					botoes <= memoria(j);
					wait for TbButtonOnWait * TbPeriod;
					botoes <= TbZero;
					wait for TbButtonOffWait * TbPeriod;
				end loop;
				wait for TbButtonOnWait*TbPeriod;
			end loop;
			-- Erra
			botoes <= memoria(9);
			wait for TbButtonOnWait * TbPeriod;
			botoes <= TbZero;
			wait for TbButtonOffWait * TbPeriod;
			--Aperta varios botões para para testar se ignora
			for i in 0 to 2 loop
				for j in 0 to i loop
					botoes <= memoria(j);
					wait for TbButtonOnWait * TbPeriod;
					botoes <= TbZero;
					wait for TbButtonOffWait * TbPeriod;
				end loop;
			end loop;
			
			--Teste acerta tudo
			jogar <= '1';
			wait for TbButtonOnWait*TbPeriod;
			jogar <= '0';
			wait for TbButtonOnWait*TbPeriod;
			
			for i in 0 to 15 loop
				for j in 0 to i loop
					botoes <= memoria(j);
					wait for TbButtonOnWait * TbPeriod;
					botoes <= TbZero;
					wait for TbButtonOffWait * TbPeriod;
				end loop;
				wait for TbButtonOnWait*TbPeriod;
			end loop;
			for j in 0 to 15 loop
				botoes <= memoria(j);
				wait for TbButtonOnWait * TbPeriod;
				botoes <= TbZero;
				wait for TbButtonOffWait * TbPeriod;
			end loop;
			wait for TbButtonOnWait*TbPeriod;

			--Inicio do jogo
			jogar <= '1';
			wait for TbButtonOnWait*TbPeriod;
			jogar <= '0';
			wait for TbButtonOnWait*TbPeriod;
			
			--Teste ate o 3 e erra por tempo
			for i in 0 to 3 loop
				for j in 0 to i loop
					botoes <= memoria(j);
					wait for TbButtonOnWait * TbPeriod;
					botoes <= TbZero;
					wait for TbButtonOffWait * TbPeriod;
				end loop;
				wait for TbButtonOnWait*TbPeriod;
			end loop;
			-- Erra
			wait for TbPeriod * 5050;

		TbSimulation <= '0';
		wait;
	end process;
		
end architecture;
