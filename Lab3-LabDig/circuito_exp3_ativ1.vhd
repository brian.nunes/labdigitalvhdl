library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram_16x4 is
   port (
       endereco     : in  std_logic_vector(3 downto 0);
       dado_entrada : in  std_logic_vector(3 downto 0);
       we           : in  std_logic;
       ce           : in  std_logic;
       dado_saida   : out std_logic_vector(3 downto 0)
    );
end entity ram_16x4;

architecture ram1 of ram_16x4 is
  type   arranjo_memoria is array(0 to 15) of std_logic_vector(3 downto 0); --tipo de vetor de vetores de bits da memoria
  signal memoria : arranjo_memoria; --signal do tipo da memoria
  attribute ram_init_file: string; --variavel para nome do arquivo de alimentação da ram
  attribute ram_init_file of memoria: signal is "ram_conteudo_inicial.mif"; --atribuicao do nome do arquivo
begin

  process(ce, we, endereco, memoria)
  begin
    if ce = '0' then -- dado armazenado na subida de "we" com "ce=0"
      if rising_edge(we) --espera a subida do we para escrever
          then memoria(to_integer(unsigned(endereco))) <= dado_entrada; --faz a escrita no endereco fornecido
      end if;
    end if;
    dado_saida <= memoria(to_integer(unsigned(endereco))); --retorna o dado do endereco fornecido
  end process;
end architecture ram1;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity contador_163 is
    port (
         clock : in  std_logic;
         clr   : in  std_logic;
         ld    : in  std_logic;
         ent   : in  std_logic;
         enp   : in  std_logic;
         D     : in  std_logic_vector (3 downto 0);
         Q     : out std_logic_vector (3 downto 0);
         rco   : out std_logic 
    );
 end contador_163;
 
 architecture comportamental of contador_163 is
   signal IQ: integer range 0 to 15;
 begin
   
   process (clock,IQ)
   begin
 
     if clock'event and clock='1' then
       if clr='0' then   IQ <= 0; 
       elsif ld='0' then IQ <= to_integer(unsigned(D));
       elsif ent='1' and enp='1' then
         if IQ=15 then   IQ <= 0; 
         else            IQ <= IQ + 1; 
         end if;
       else              IQ <= IQ;
       end if;
     end if;
     
     if IQ=15 and ent='1' then rco <= '1'; 
     else                      rco <= '0'; 
     end if;
 
     Q <= std_logic_vector(to_unsigned(IQ, Q'length));
 
   end process;
 end comportamental;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity comparador is
    port (
      i_A3   : in  std_logic;
      i_B3   : in  std_logic;
      i_A2   : in  std_logic;
      i_B2   : in  std_logic;
      i_A1   : in  std_logic;
      i_B1   : in  std_logic;
      i_A0   : in  std_logic;
      i_B0   : in  std_logic;
      i_AGTB : in  std_logic;
      i_ALTB : in  std_logic;
      i_AEQB : in  std_logic;
      o_AGTB : out std_logic;
      o_ALTB : out std_logic;
      o_AEQB : out std_logic
    );
end entity comparador;
  
architecture dataflow of comparador is
    signal agtb : std_logic;
    signal aeqb : std_logic;
    signal altb : std_logic;
  begin
    -- equacoes dos sinais: pagina 462, capitulo 6 do livro-texto
    -- Wakerly, J.F. Digital Design - Principles and Practice, 4th Edition
    -- veja tambem datasheet do CI SN7485 (Function Table) 
    agtb <= (i_A3 and not(i_B3)) or
            (not(i_A3 xor i_B3) and i_A2 and not(i_B2)) or
            (not(i_A3 xor i_B3) and not(i_A2 xor i_B2) and i_A1 and not(i_B1)) or
            (not(i_A3 xor i_B3) and not(i_A2 xor i_B2) and not(i_A1 xor i_B1) and i_A0 and not(i_B0));
    aeqb <= not((i_A3 xor i_B3) or (i_A2 xor i_B2) or (i_A1 xor i_B1) or (i_A0 xor i_B0));
    altb <= not(agtb or aeqb);
    -- saidas
    o_AGTB <= agtb or (aeqb and (not(i_AEQB) and not(i_ALTB)));
    o_ALTB <= altb or (aeqb and (not(i_AEQB) and not(i_AGTB)));
    o_AEQB <= aeqb and i_AEQB;
    
end architecture dataflow;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity circuito_exp3_ativ1 is
  port (
        clock		: in  std_logic;
        reset		: in  std_logic;
        enable		: in  std_logic;
        escreve     : in  std_logic;
        chaves		: in  std_logic_vector (3 downto 0);
        igual		: out std_logic;
        fim         : out std_logic;
        db_contagem : out std_logic_vector (3 downto 0);
        db_memoria : out std_logic_vector (3 downto 0)
);
end circuito_exp3_ativ1;

architecture structural of circuito_exp3_ativ1 is

  component ram_16x4
        port(
            endereco     : in  std_logic_vector(3 downto 0);
            dado_entrada : in  std_logic_vector(3 downto 0);
            we           : in  std_logic;
            ce           : in  std_logic;
            dado_saida   : out std_logic_vector(3 downto 0)
        );
  end component;

  component comparador
    port(
	  i_A3   : in  std_logic;
      i_B3   : in  std_logic;
      i_A2   : in  std_logic;
      i_B2   : in  std_logic;
      i_A1   : in  std_logic;
      i_B1   : in  std_logic;
      i_A0   : in  std_logic;
      i_B0   : in  std_logic;
      i_AGTB : in  std_logic;
      i_ALTB : in  std_logic;
      i_AEQB : in  std_logic;
      o_AGTB : out std_logic;
      o_ALTB : out std_logic;
      o_AEQB : out std_logic
	);
  end component;
  
  component contador_163
    port(
	  clock : in  std_logic;
      clr   : in  std_logic;
      ld    : in  std_logic;
      ent   : in  std_logic;
      enp   : in  std_logic;
      D     : in  std_logic_vector (3 downto 0);
      Q     : out std_logic_vector (3 downto 0);
      rco   : out std_logic 
	);
  end component;

  signal sinal_chave, contagem, db_mem: std_logic_vector (3 downto 0);
  signal not_reset, ck: std_logic;
begin

    RAM: ram_16x4 port map (
        endereco     => contagem,
        dado_entrada => sinal_chave,
        we           => escreve,
        ce           => '0',
        dado_saida   => db_mem
    );

  CMP: comparador port map (
    i_A3 => db_mem(3),
	  i_B3 => sinal_chave(3),
    i_A2 => db_mem(2),
	  i_B2 => sinal_chave(2),
    i_A1 => db_mem(1),
	  i_B1 => sinal_chave(1),
    i_A0 => db_mem(0),
	  i_B0 => sinal_chave(0),
	  i_AGTB => '0',
	  i_ALTB => '0',
	  i_AEQB => '1',
	  o_AGTB => open,
	  o_ALTB => open,
	  o_AEQB => igual
  );
  
  COUNT: contador_163 port map (
	  clock => ck,
    clr   => not_reset,
	  ld    => '1',
	  ent   => '1',
	  enp   => enable,
	  D     => "0000",
    Q     => contagem,
	  rco   => fim
  );

  ck <= clock;
  not_reset <= not(reset);
  sinal_chave <= chaves;
  db_memoria <= db_mem;
  db_contagem <= contagem;

end architecture;