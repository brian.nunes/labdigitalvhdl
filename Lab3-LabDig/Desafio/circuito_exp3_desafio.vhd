library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity circuito_exp3_desafio is
  port (
        clock		: in  std_logic;
        zeraL		: in  std_logic;
        zeraC		: in  std_logic;
        enableL		: in  std_logic;
        enableC		: in  std_logic;
        escreve     : in  std_logic;
        chaves		: in  std_logic_vector (3 downto 0);
        chavesIgualMemoria		: out std_logic;
        enderecoMenorLimite		: out std_logic;
        fimL         : out std_logic;
        fimC         : out std_logic;
        db_contagem : out std_logic_vector (6 downto 0);
        db_memoria : out std_logic_vector (6 downto 0);
        db_limite : out std_logic_vector (6 downto 0)
);
end circuito_exp3_desafio;

architecture structural of circuito_exp3_desafio is

  component hexa7seg
    port (
      hexa : in  std_logic_vector(3 downto 0);
      sseg : out std_logic_vector(6 downto 0)
    );
  end component;

  component ram_16x4
        port(
            endereco     : in  std_logic_vector(3 downto 0);
            dado_entrada : in  std_logic_vector(3 downto 0);
            we           : in  std_logic;
            ce           : in  std_logic;
            dado_saida   : out std_logic_vector(3 downto 0)
        );
  end component;

  component comparador_85
    port(
	    i_A3   : in  std_logic;
      i_B3   : in  std_logic;
      i_A2   : in  std_logic;
      i_B2   : in  std_logic;
      i_A1   : in  std_logic;
      i_B1   : in  std_logic;
      i_A0   : in  std_logic;
      i_B0   : in  std_logic;
      i_AGTB : in  std_logic;
      i_ALTB : in  std_logic;
      i_AEQB : in  std_logic;
      o_AGTB : out std_logic;
      o_ALTB : out std_logic;
      o_AEQB : out std_logic
	);
  end component;
  
  component contador_163
    port(
	  clock : in  std_logic;
      clr   : in  std_logic;
      ld    : in  std_logic;
      ent   : in  std_logic;
      enp   : in  std_logic;
      D     : in  std_logic_vector (3 downto 0);
      Q     : out std_logic_vector (3 downto 0);
      rco   : out std_logic 
	);
  end component;

  signal sinal_chave, s_endereco, s_limite, s_dado: std_logic_vector (3 downto 0);
  signal not_zeraL, not_zeraC, ck: std_logic;
begin

    HEX1: hexa7seg port map (
      hexa => s_limite,
      sseg => db_limite
    );

    HEX2: hexa7seg port map (
      hexa => s_endereco,
      sseg => db_contagem
    );

    HEX3: hexa7seg port map (
      hexa => s_dado,
      sseg => db_memoria
    );

    MemJog: ram_16x4 port map (
        endereco     => s_endereco,
        dado_entrada => sinal_chave,
        we           => escreve,
        ce           => '0',
        dado_saida   => s_dado
    );

    CompEnd: comparador_85 port map (
      i_A3 => s_limite(3),
      i_B3 => s_endereco(3),
      i_A2 => s_limite(2),
      i_B2 => s_endereco(2),
      i_A1 => s_limite(1),
      i_B1 => s_endereco(1),
      i_A0 => s_limite(0),
      i_B0 => s_endereco(0),
      i_AGTB => '0',
      i_ALTB => '0',
      i_AEQB => '1',
      o_AGTB => enderecoMenorLimite,
      o_ALTB => open,
      o_AEQB => open
    );

    CompJog: comparador_85 port map (
      i_A3 => s_dado(3),
      i_B3 => sinal_chave(3),
      i_A2 => s_dado(2),
      i_B2 => sinal_chave(2),
      i_A1 => s_dado(1),
      i_B1 => sinal_chave(1),
      i_A0 => s_dado(0),
      i_B0 => sinal_chave(0),
      i_AGTB => '0',
      i_ALTB => '0',
      i_AEQB => '1',
      o_AGTB => open,
      o_ALTB => open,
      o_AEQB => chavesIgualMemoria
    );
  
    ContLmt: contador_163 port map (
      clock => ck,
      clr   => not_zeraL,
      ld    => '1',
      ent   => '1',
      enp   => enableL,
      D     => "0000",
      Q     => s_limite,
      rco   => fimL
    );

    ContEnd: contador_163 port map (
      clock => ck,
      clr   => not_zeraC,
      ld    => '1',
      ent   => '1',
      enp   => enableC,
      D     => "0000",
      Q     => s_endereco,
      rco   => fimC
    );

  ck <= clock;
  not_zeraL <= not(zeraL);
  not_zeraC <= not(zeraC);
  sinal_chave <= chaves;

end architecture;