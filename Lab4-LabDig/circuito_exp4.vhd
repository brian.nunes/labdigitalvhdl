library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity circuito_exp4 is
    port(
        clock       :   in std_logic;
        reset       :   in std_logic;
        iniciar     :   in std_logic;
        chaves       :   in std_logic_vector (3 downto 0);
        pronto      :   out std_logic;
        db_igual    :   out std_logic;
        db_memoria  :   out std_logic_vector (6 downto 0);
        db_contagem :   out std_logic_vector (6 downto 0);
        db_estado   :   out std_logic_vector (6 downto 0)
    );
end entity;

architecture arch of circuito_exp4 is

    component hexa7seg
        port (
            hexa : in  std_logic_vector(3 downto 0);
            sseg : out std_logic_vector(6 downto 0)
        );
    end component;

    component fluxo_dados
        port (
            clock		: in  std_logic;
            zeraC		: in  std_logic;
            contaC		: in  std_logic;
            escreveM     : in  std_logic;
            chaves		: in  std_logic_vector (3 downto 0);
            igual		: out std_logic;
            fimC         : out std_logic;
            db_contagem : out std_logic_vector (3 downto 0);
            db_memoria : out std_logic_vector (3 downto 0)
        );
    end component;

    component unidade_controle
        port ( 
            clock:     in  std_logic; 
            reset:     in  std_logic; 
            iniciar:   in  std_logic;
            fim:       in  std_logic;
            zera:      out std_logic;
            conta:     out std_logic;
            pronto:    out std_logic;
            db_estado: out std_logic_vector(3 downto 0)
        );
    end component;

    signal db_mem4, db_conta4, db_estado4: std_logic_vector(3 downto 0);
    signal ck, db_fim, db_zera, db_conta: std_logic;
begin

    HEX0: hexa7seg port map (
        hexa => db_mem4,
        sseg => db_memoria
    );

    HEX1: hexa7seg port map (
        hexa => db_conta4,
        sseg => db_contagem
    );

    HEX2: hexa7seg port map (
        hexa => db_estado4,
        sseg => db_estado
    );

    UC: unidade_controle port map (
        clock => ck,
        reset => reset,
        iniciar => iniciar,
        fim => db_fim,
        zera => db_zera,
        conta => db_conta,
        pronto => pronto,
        db_estado => db_estado4
    );

    FD: fluxo_dados port map(
        clock => ck,
        zeraC => db_zera,
        contaC => db_conta,
        escreveM => '1',
        chaves => chaves,
        igual => db_igual,
        fimC => db_fim,
        db_contagem => db_conta4,
        db_memoria => db_mem4
    );

    ck <= clock;
end arch;