library ieee;
use ieee.std_logic_1164.all;

entity unidade_controle is 
  port ( 
    clock:     in  std_logic; 
    reset:     in  std_logic; 
    iniciar:   in  std_logic;
    fim:       in  std_logic;
    zera:      out std_logic;
    conta:     out std_logic;
    pronto:    out std_logic;
    db_estado: out std_logic_vector(3 downto 0)
  );
end entity;

architecture fsm of unidade_controle is
  type t_estado is (A, B, C, D, E, M);
  signal Eatual, Eprox: t_estado;
begin
  -- memoria de estado
  process (clock,reset)
  begin
    if reset='1' then
      Eatual <= A;
    elsif clock'event and clock = '1' then
      Eatual <= Eprox; 
    end if;
  end process;

  -- logica de proximo estado
  Eprox <=
      A when  Eatual=A and iniciar='0' else
      B when  Eatual=A and iniciar='1' else
		  M when  Eatual=B else
      C when  Eatual=M else
      D when  Eatual=C and fim='0' else
      E when  Eatual=C and fim='1' else
      M when  Eatual=D else
      A when  Eatual=E else
      A;

  -- logica de saída (maquina de Moore)
  with Eatual select
    zera <=   '0' when A | C | D | E | M,
              '1' when B,
              '0' when others;

  with Eatual select
    conta <=  '0' when A | B | C | E | M,
              '1' when D,
              '0' when others;

  with Eatual select
    pronto <= '0' when A | B | C | D | M,
              '1' when E,
              '0' when others;

  -- saida de depuracao (db_estado)
  with Eatual select
    db_estado <= "0000" when A,      -- 0
                 "1011" when B,      -- B
                 "1100" when C,      -- C
                 "1101" when D,      -- D
                 "1110" when E,      -- E
					      "0111" when M,      -- M
                 "1111" when others; -- F
end fsm;