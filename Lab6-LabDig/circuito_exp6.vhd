library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity circuito_exp6 is
    port (
        clock : in std_logic;
        reset : in std_logic;
        jogar : in std_logic;
        botoes : in std_logic_vector (3 downto 0);
        leds : out std_logic_vector (3 downto 0);
        pronto : out std_logic;
        ganhou : out std_logic;
        perdeu : out std_logic
    );
end entity;

architecture arch of circuito_exp6 is

    component fluxo_dados
        port (
            clock		    : in  std_logic;
            zeraL 	    : in  std_logic;
            contaL		  : in  std_logic;
            zeraE 	    : in  std_logic;
            resetGeral  : in  std_logic;
            contaE		  : in  std_logic;
            zeraR		    : in  std_logic;
            escreve     : in  std_logic;
            botoes		  : in  std_logic_vector (3 downto 0);
            registraR	  : in  std_logic;
            fimL        : out std_logic;
            fimE        : out std_logic;
            jogada_feita  : out std_logic;
            db_limite  : out std_logic_vector (3 downto 0);
            db_jogada  : out std_logic_vector (3 downto 0);
            db_memoria  : out std_logic_vector (3 downto 0);
            db_contagem : out std_logic_vector (3 downto 0);
            chavesIgualMemoria		    : out std_logic;  
            enderecoIgualOuMenorLimite		    : out std_logic;
            enderecoIgualLimite		    : out std_logic 
        );
    end component;

    component unidade_controle
        port ( 
            clock:     in  std_logic; 
            reset:     in  std_logic; 
            jogar:   in  std_logic;
            igualContELmt:       in  std_logic;
            fimL:       in  std_logic;
            igual:     in  std_logic;
            jogada:    in  std_logic;
            pronto:    out std_logic;
            zeraL:      out std_logic;
            zeraE:      out std_logic;
            zeraR:      out std_logic;
            registra:  out std_logic;
            contaE:     out std_logic;
            contaL:     out std_logic;
            acertou:   out std_logic;
            errou:     out std_logic;
            db_estado: out std_logic_vector(3 downto 0)
        );
    end component;

    signal ck, db_fimL, db_fimE, db_zeraR, db_zeraE, db_zeraL, db_contaL, db_contaE, db_i, db_registra, db_jogada_feita, db_endI, db_endMouI, s_reset: std_logic;
    signal db_mem4, db_conta4, db_estado4, db_chaves_or, db_or_geral, db_jogada4, db_lmt, db_botoes: std_logic_vector (3 downto 0);
begin

    UC: unidade_controle port map (    
        jogada => db_jogada_feita,
        registra => db_registra,
        clock => ck,
        reset => s_reset,
        jogar => jogar,
        igual => db_i,
        igualContELmt => db_endI,
        fimL => db_fimL,
        zeraL => db_zeraL,
        zeraE => db_zeraE,
        zeraR => db_zeraR,
        contaE => db_contaE,
        contaL => db_contaL,
        acertou => ganhou,
        errou => perdeu,
        db_estado => db_estado4,
		pronto => pronto
    );

    FD: fluxo_dados port map(
        clock => ck,
        zeraL => db_zeraL,
        contaL => db_contaL,
        fimL => db_fimL,
        zeraE => db_zeraE,
        contaE => db_contaE,
        resetGeral => s_reset,
        fimE => db_fimE,
        zeraR => db_zeraR,
        registraR => db_registra,
        escreve => '1',
        botoes => db_botoes,
        chavesIgualMemoria => db_i,
        jogada_feita => db_jogada_feita,
        db_limite => db_lmt,
        db_contagem => db_conta4,
        db_memoria => db_mem4,
        db_jogada => db_jogada4,
        enderecoIgualOuMenorLimite => db_endMouI,
        enderecoIgualLimite	=> db_endI
    );
    s_reset <= reset;
    db_botoes <= botoes;
    leds <= db_botoes;
    ck <= clock;
end arch;