------------------------------------------------------------------
-- Arquivo   : unidade_controle.vhd
-- Projeto   : Experiencia 04 - Projeto de uma Unidade de Controle
------------------------------------------------------------------
-- Descricao : codificação da máquina de estados em VHDL 
--             > para mais informação, consulte o livro
--               VHDL Descricao e Sintese de Circuitos Digitais
--               Roberto D'Amore, LTC Editora.
--               ou acesse a postagem do prof, Bruno Albertini em
--               https://balbertini.github.io/vhdl_fsm-pt_BR.html
------------------------------------------------------------------
-- Revisoes  :
--     Data        Versao  Autor             Descricao
--     21/01/2020  1.0     Edson Midorikawa  criacao
------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity unidade_controle is 
  port ( 
    clock:     in  std_logic; 
    reset:     in  std_logic; 
    jogar:   in  std_logic;
    igualContELmt:       in  std_logic;
    fimL:       in  std_logic;
    igual:     in  std_logic;
    jogada:    in  std_logic;
    pronto:    out std_logic;
    uc_escreve:    out std_logic;
    zeraL:      out std_logic;
    zeraE:      out std_logic;
    zeraR:      out std_logic;
    registra:  out std_logic;
    contaE:     out std_logic;
    contaL:     out std_logic;
    acertou:   out std_logic;
    errou:     out std_logic;
    db_estado: out std_logic_vector(3 downto 0)
  );
end entity;

architecture fsm of unidade_controle is
  type t_estado is (A, B, C, D, E, F, G, H, I, J, K, L, M, N);
  signal Eatual, Eprox: t_estado;
begin
  -- memoria de estado
  process (clock,reset)
  begin
    if reset='1' then
      Eatual <= A;
    elsif clock'event and clock = '1' then
      Eatual <= Eprox; 
    end if;
  end process;

  -- logica de proximo estado
  Eprox <=
      A when  Eatual=A and jogar='0' else
      B when  Eatual=A and jogar='1' else
      C when  Eatual=B else
      D when  Eatual=C else
      D when  Eatual=D and jogada='0' else
      D when  Eatual=D and jogada = 'U' else
      E when  Eatual=D and jogada='1'else
      F when  Eatual=E else
      G when  Eatual=F and igualContELmt='0' and igual='1' else
      I when  Eatual=F and igualContELmt='1' and igual='1' else
      H when  Eatual=F and igualContELmt='0' and igual='0' else
      H when  Eatual=F and igualContELmt='1' and igual='0' else
      D when  Eatual=G else
      B when  Eatual=H and jogar='1' else
      H when  Eatual=H and jogar='0' else
      N when  Eatual=I and fimL='1'else
      J when  Eatual=I and fimL='0' else
      K when  Eatual=J else
      L when  Eatual=K and jogada='1' else
      K when  Eatual=K and jogada='0' else
      M when  Eatual=L else
      B when  Eatual=N and jogar='1' else
      N when  Eatual=N and jogar='0' else
      C when  Eatual=M else
      B;

  -- logica de saída (maquina de Moore)
  with Eatual select
    registra <=   '0' when A | B | C | D | F | G | H | I | J | K,
                  '1' when E | L,
                  '0' when others;

  with Eatual select
    zeraL <=   '0' when A | C | D | E | F | G | H | I | J | K,
              '1' when B,
              '0' when others;

  with Eatual select
    zeraE <=   '0' when A | B | D | E | F | G | H | I | J | K,
              '1' when C,
              '0' when others;
  
  with Eatual select
    zeraR <=   '0' when A | B | D | E | F | G | H | I | J | K,
              '1' when C,
              '0' when others;

  with Eatual select
    contaE <=  '0' when A | B | C | D | E | F | H | I | K,
              '1' when G | J,
              '0' when others;

  with Eatual select
    contaL <=  '0' when A | B | C | D | E | F | G | H | I | J,
              '1' when M,
              '0' when others;

  with Eatual select
    acertou <= '0' when A | B | C | D | E | F | G | H | I | K,
              '1' when N,
              '0' when others;

  with Eatual select
    errou <= '0' when A | B | C | D | E | F | G | I | J | K,
              '1' when H,
              '0' when others;
  
  with Eatual select
    pronto <= '0' when A | B | C | D | E | F | G | I | K,
              '1' when N | H,
              '0' when others;

    with Eatual select
    uc_escreve <= '1' when A | B | C | D | E | F | G | I | J | K,
              '0' when M,
              '1' when others;

  -- saida de depuracao (db_estado)
  with Eatual select
    db_estado <= "0001" when A,      -- 1
                 "0010" when B,      -- 2
                 "0011" when C,      -- 3
                 "0100" when D,      -- 4
                 "0101" when E,      -- 5
                 "0110" when F,      -- 6
                 "0111" when G,      -- 7
                 "1000" when H,      -- 8
                 "1001" when I,      -- 9
                 "1010" when J,      -- A
                 "1011" when K,      -- B
                 "1100" when L,      -- C
                 "1101" when M,      -- D
                 "1110" when N,      -- E
                 "0000" when others; -- 0
end fsm;   