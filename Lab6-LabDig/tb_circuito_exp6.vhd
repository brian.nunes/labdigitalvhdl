library ieee;
use ieee.std_logic_1164.all;

entity tb_circuito_exp6 is
end tb_circuito_exp6;

architecture tb of tb_circuito_exp6 is

    component circuito_exp6
        port (clock  : in std_logic;
              reset  : in std_logic;
              jogar  : in std_logic;
              botoes : in std_logic_vector (3 downto 0);
              leds   : out std_logic_vector (3 downto 0);
              pronto : out std_logic;
              ganhou : out std_logic;
              perdeu : out std_logic);
    end component;

    signal clock  : std_logic;
    signal reset  : std_logic;
    signal jogar  : std_logic;
    signal botoes : std_logic_vector (3 downto 0);
    signal leds   : std_logic_vector (3 downto 0);
    signal pronto : std_logic;
    signal ganhou : std_logic;
    signal perdeu : std_logic;

    constant TbPeriod : time := 10000 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : circuito_exp6
    port map (clock  => clock,
              reset  => reset,
              jogar  => jogar,
              botoes => botoes,
              leds   => leds,
              pronto => pronto,
              ganhou => ganhou,
              perdeu => perdeu);

    -- Clock generation
    TbClock <= not TbClock after 500 ns when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clock is really your main clock signal
    clock <= TbClock;

    stimuli : process
    begin
        jogar <= '0';
        -- Reset generation
        reset <= '1';
        wait for 2000 ns;
        reset <= '0';
        wait for 2000 ns;


        --Inicio do jogo
        jogar <= '1';
        wait for 2000 ns;
        jogar <= '0';
        wait for 2000 ns;

        -- Teste acertar até o 2 e errar
        -- primeira jogada da primeira rodada
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- salva segunda
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        -- primeira jogada
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- segunda jogada
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- salva terceira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        -- primeira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- segunda
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- terceira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- salva quarta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        -- primeira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- segunda
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- terceira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quarta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- salva quinta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        -- primeira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- segunda
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- terceira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quarta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quinta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- salva sexta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        -- primeira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- segunda
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- terceira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quarta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quinta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- sexta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- salva setima
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        -- primeira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- segunda
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- terceira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quarta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quinta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- sexta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- setima
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- salva oitava
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        -- primeira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- segunda
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- terceira
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quarta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- quinta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- sexta
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- setima
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- oitava
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;
        -- salva nova
        botoes <= "0001";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        --erra
        botoes <= "0010";
        wait for 5 * TbPeriod;
        botoes <= "0000";
        wait for 3 * TbPeriod;

        --Inicio do jogo
        wait for 3000 ns;
        jogar <= '1';
        wait for 4000 ns;
        jogar <= '0';
        wait for 4000 ns;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_circuito_exp6 of tb_circuito_exp6 is
    for tb
    end for;
end cfg_tb_circuito_exp6;